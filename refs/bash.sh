#!/bin/bash

# run a script with:
# $ bash bash.sh
# or:
# $ chmod +x bash.sh
# $ bash.sh

# in bash, 0 is true/success and 1 (or other error codes) are false/failure

# limitations: bash doesn't support floats or multi-dimensional arrays

# string comparison (note that spacing around the brackets is required):
if [[ "apple" == "apple" ]]
then
	echo "apple matched";
fi

# single quotes vs double quotes:
VARA=text
echo 'hi $VARA there' # hi $VARA there
echo "hi $VARA there" # hi text there

var_b=moretext
echo $var_b

# math
A=2
B=3
((C=$A+$B))
echo $C

# while loop
while [[ $C > 0 ]]
do
	echo $C
	((C-=1))
done

# 1-d array
declare -a ARRY
#ARRY=( hello howdy hi )
#ARRY=( "hello" "howdy" "hi" ) # also works
ARRY=( 10 11 howdy )

# for loop over array
#for ELT in $ARRY; # doesn't work for some reason
for ELT in ${ARRY[@]};
do
	echo $ELT
done






function(print_variables)
	message(STATUS "==========")
	message(STATUS "current script file is ${CMAKE_CURRENT_LIST_FILE}")
	message(STATUS "current script directory is ${CMAKE_CURRENT_LIST_DIR}")
	message(STATUS "")

	message(STATUS "CMake source directory is ${CMAKE_SOURCE_DIR}")
	message(STATUS "CMake binary directory is ${CMAKE_BINARY_DIR}")
	message(STATUS "")

	message(STATUS "current project name, version are ${PROJECT_NAME}, ${PROJECT_VERSION}")
	message(STATUS "source directory of project ${PROJECT_NAME} is ${PROJECT_SOURCE_DIR}")
	message(STATUS "binary directory of project ${PROJECT_NAME} is ${PROJECT_BINARY_DIR}")
	message(STATUS "")

	message(STATUS "current CMake source directory is ${CMAKE_CURRENT_SOURCE_DIR}")
	message(STATUS "current CMake binary directory is ${CMAKE_CURRENT_BINARY_DIR}")
	message(STATUS "==========")
endfunction()
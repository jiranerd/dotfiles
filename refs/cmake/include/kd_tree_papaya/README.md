NOTE: This directory is for headers which declare functions we are exposing to consumers of this library.
If we are simply writing a command line utility, this directory may not be needed.
https://medium.com/heuristics/c-application-development-part-1-project-structure-454b00f9eddc

#include <iostream>
#include "foo.h"

#include <boost/accumulators/accumulators.hpp> // works

using std::cout;
using std::endl;

int main(int argv, char** argc){

	if (argv == 2)
	{
		cout << argc[1] << endl;
	}
	else
	{

		cout << argv << " " << argc[0] << endl;
		cout << "hello world" << endl;

		// Declared in command_line CMakeLists.txt.
		cout << "num listeners: " << NUM_LISTENERS << endl;

		// Declared in data_generator CMakeLists.txt.
		cout << "num recipients: " << NUM_RECIPIENTS << endl;
		cout << "num respondees: " << NUM_RESPONDEES << endl;

		// Declared in data_generator CMakeLists.txt.
		// Not valid. NUM_ATTENDEES is declared PRIVATE in data_generator CMakeLists.txt.
		//cout << "num attendees: " << NUM_ATTENDEES << endl;

		cout << "foo:" << foo(3) << endl;

	}

	// Can make ctest fail by returning -1.
	//return -1;
}
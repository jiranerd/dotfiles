
//#include <iostream>
//
//using std::cout;
//using std::endl;
//
//int main()
//{
//	cout << "running test 1" << endl;
//}



// https://www.boost.org/doc/libs/1_72_0/libs/test/doc/html/boost_test/intro.html

#define BOOST_TEST_MODULE My Test
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_CASE(first_test)
{
    int i = 1;
    BOOST_TEST(i);
    BOOST_TEST(i == 2);
    BOOST_TEST(i == 1);
}


// https://www.boost.org/doc/libs/1_72_0/libs/test/doc/html/boost_test/intro.html

#define BOOST_TEST_MODULE My Test
#include <boost/test/included/unit_test.hpp>
#include "foo.h"

BOOST_AUTO_TEST_CASE(first_test)
{
    BOOST_TEST(foo(2) = 4);
}
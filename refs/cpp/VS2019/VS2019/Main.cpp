

// see notes.txt for general commentary.

// following this bug which affects semicolon and intellisense:
// https://developercommunity.visualstudio.com/content/problem/750655/pressing-semi-colon-closes-intellisense-instead-of.html

#include "main.h"

//using namespace std; 
	// pulls large number of declarations into scope.
using std::cout;
using std::endl;
using std::string;

// normally would put each of these function declarations in its own .h file,
// because the functions live in separate .ccp files.
// leaving them here to keep file count down.
void scratch();
void variables();
void strings();
void control_flow();
void functions();
void structs();
void classes();
void special_member_functions();
void memory_management();
void smart_pointers();
void arrays();
void vectors_and_functions();
void vector_of_objects();
void templates();
void containers();
void performance();
void exceptions();
void linked_list();

void section(string s) { cout << "\n**********\n* " + s + "\n**********\n\n"; }

int main()
{

	section("scratch");
	scratch();

	section("variables");
	variables();

	section("strings");
	strings();

	section("control_flow");
	control_flow();

	section("functions");
	functions();

	section("structs");
	structs();

	section("classes");
	classes();

	section("special_member_functions");
	special_member_functions();

	section("memory_management");
	memory_management();

	section("smart_pointers");
	smart_pointers();

	section("arrays");
	arrays();

	section("templates");
	templates();

	section("containers");
	containers();

	section("vectors_and_functions");
	vectors_and_functions();

	section("vector_of_objects");
	vector_of_objects();

	section("exceptions");
	exceptions();

	section("linked_list");
	linked_list();

	cout << "bye" << endl;

	// asserts

	//**********
	// padding (would go naturally with structs?)
	//**********

	//**********
	// function overloading
	//**********

	//**********
	// custom operators
	//**********

	//**********
	// regex (put with strings?)
	//**********

	//**********
	// json
	//**********

	// json to/from file

	//**********
	// threading and async
	//**********


	// "this" keyword
	// lambda functions
	// async
	// web requests
	// inter-process communication
	// namespaces

	section("performance");
	performance();

	//system("pause");
	return 0;
}
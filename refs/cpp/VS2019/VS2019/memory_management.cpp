

#include <iostream>
#include <string>
using std::cout;
using std::endl;
using std::to_string;

class Foo
{
public:
	int a;
	int b;

	~Foo()
	{
		cout << "calling destructor ~Foo" << endl;
	}
};

void memory_management()
{
	cout << "hi" << endl;

	// these variables are allocated on the stack.
	// for Foo f, the dtor will be called when the function exits, 
	// even if an exception is raised or the function returns early. (or was this unique_ptr?)
	int a{ 3 };
	int r[]{ 10, 11, 12 };
	Foo f{ 1, 2 };

	// malloc, free
	// malloc returns a null pointer. it must be cast.
	// malloc returns NULL if memory allocation fails. not checking for null causes compiler warning.
	// free does NOT call the dtor for Foo.
	int* ptr_i = (int*)malloc(sizeof(int));
	int* ptr_r = (int*)malloc(sizeof(int) * 3);
	Foo* ptr_f = (Foo*)malloc(sizeof(Foo));

	*ptr_i = 3;
	ptr_r[1] = 4;
	ptr_f->a = 5;
	cout << to_string(*ptr_i) << "," << to_string(ptr_r[1]) << "," << to_string(ptr_f->a) << endl;

	free(ptr_i);
	free(ptr_r);
		// the size of the memory to be freed is recorded and stored when malloc is called.
		// https://stackoverflow.com/a/1518718/2512141
	free(ptr_f);



	// new, delete
	// new returns a typed pointer.
	// delete calls the dtor for Foo.

	//int* new_i = new int{ 8 };
		// this is valid syntax.
	int* new_i = new int;
	int* new_r = new int[3];
	Foo* new_f = new Foo{ 11, 12 };

	*new_i = 8;
	new_r[1] = 9;
	cout << to_string(*new_i) << "," << to_string(new_r[1]) << "," << to_string(new_f->a) << endl;

	delete new_i;
	delete[] new_r;
		// why do we need different syntax for array delete? what happens if we use scalar delete?
		// again, length of array is noted at allocation time.
		// https://stackoverflow.com/a/703706/2512141
		// https://isocpp.org/wiki/faq/freestore-mgmt#num-elems-in-new-array
	delete new_f;


	// custom memory pools with placement new?
	// https://stackoverflow.com/questions/222557/what-uses-are-there-for-placement-new/222578#222578


	cout << "about to exit function memory_management" << endl;
}

#include <iostream>
#include <string>
//#include <utility>

#include "tea_set.h"

using std::cout;
using std::endl;
using std::to_string;

// copied directly from https://en.wikipedia.org/wiki/Special_member_functions
// there is a lot to unpack here...
// explicit keyword?
// && notation?
// other?
// why does example include <string> and <utility>? program compiles and runs without them.

// might replace all of this with my own examples crafted from here:
// http://www.cplusplus.com/doc/tutorial/classes2/

// some useful patterns for constructing normal, immoveable, container, and resource_handle classes
// by specifying/modifying the special member functions.
// https://foonathan.net/special-member-chart/
// https://foonathan.net/2019/02/special-member-functions/
// also shows use of default, delete, and noexcep on constructors.
// what about keyword explicit?

// why do we need move constructor and move assignment?
// why not just keep using the existing variable?
// https://mbevin.wordpress.com/2012/11/20/move-semantics/

// using rvalue reference for perfect forwarding.
// https://docs.microsoft.com/en-us/cpp/cpp/rvalue-reference-declarator-amp-amp?view=vs-2019

// when should we use move constructor, move assignment?
// https://stackoverflow.com/a/27026280/2512141

// Bjarne Stroustrup: "The first task of rvalue references is to allow us to implement move() without verbosity, or runtime overhead."
// http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n2027.html#Move_Semantics

// Hinnant, Stroustrup: A Brief Introduction to Rvalue References
// http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n2027.html
// "Rvalue references allow programmers to avoid logically unnecessary copying and to provide perfect forwarding functions."
// "The first task of rvalue references is to allow us to implement move() without verbosity, or rutime overhead."
// The second task of rvalue references is to allow constructors to accept both const and non-const reference arguments without writing separate constructors for each.
// (Without this, forwarding would require a number of constructors exponential in the number of args?)

// And then, further complicating issues, we have copy ellision, which doesn't quite remove need for move contstructor?
// https://akrzemi1.wordpress.com/2011/08/11/move-constructor/
// http://definedbehavior.blogspot.com/2011/08/value-semantics-copy-elision.html

// when does the compiler use a move constructor?
// "If the compiler can detect that the source of the copy constructor is not going to be used (read from or written to)
// anymore, but only destroyed, the compiler will pick the move constructor."
// https://akrzemi1.wordpress.com/2011/08/11/move-constructor/

// move semantics
// https://www.internalpointers.com/post/c-rvalue-references-and-move-semantics-beginners

// copy constructor, special member functions:
// https://en.wikipedia.org/wiki/Assignment_operator_(C%2B%2B)
// https://en.wikipedia.org/wiki/Special_member_functions
// default constructor, copy constructor, move constructor, copy assignment operator, move assignment operator, destructor


void special_member_functions()
{
	

	// parameterized constructor
	tea_set ts_a(11, 12);

	// copy constructor
	tea_set ts_A = ts_a;

	// copy assignment
	tea_set ts_c(21, 22);
	ts_c = ts_a;

	// move constructor
	tea_set ts_d = std::move(tea_set(33, 34));
		// note: tea_set(33, 34) is a temporary (aka unnamed, aka anonymous) value. (?)
		// note: this method of constructing ts_d is suboptimal, as it creates two objects.
		// tea_set ts_d = tea_set(33, 34); creates only one object.
		// (this would be equivalent to TeaSet ts_d{33, 34}; ?
		// we use std::move() to force calling of move constructor for sake of example.

	// move assignment
	tea_set ts_e(44, 45);
	tea_set ts_f(55, 56);
	ts_e = std::move(ts_f);
		// std::move returns an rvalue reference (&&).
		// we could alternatively define a custom operator + which returns an rvalue reference.
		// http://www.cplusplus.com/doc/tutorial/classes2/

}
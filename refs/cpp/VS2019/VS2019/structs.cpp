

// structs are almost identical to classes, so only a few struct examples are given here.
// only difference between structs, classes: struct members are public by default, class members are private by default.

// pointers to structs and pointers to classes work the same way?

// cover typedefs? is that still a thing in C++17?
// there's an example here, https://www.tutorialspoint.com/cplusplus/cpp_data_structures.htm
// but my code below doesn't seem to need typedefs.

// it seems like typedefs only appear with structs, but could they be used other places?
// are they associated with structs purely for historical (C) reasons?

#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::to_string;

struct ComplexNum
{
	double real;
	double imag;
};

void structs()
{
	// init with assignment
	ComplexNum a;
	a.real = 2.0;
	a.imag = 3.0;

	// init with initializer list
	ComplexNum b = { 4.0, 5.0 };

	// init with uniform initialization
	ComplexNum c{ 6.0, 7.0 };

	// init providing only one value
	ComplexNum d{ 8.0 };
		// sets d.real
	cout << "d:" << to_string(d.real) << "," << to_string(d.imag) << endl;

	// setting member values after creation with initialization list
	a = { 12.0, 13.0 };

	// pointer to a struct
	ComplexNum* ptr_h = new ComplexNum{ 1, 2 };
	cout << "h:" << to_string(ptr_h->real) << "," << to_string(ptr_h->imag) << endl;
	delete ptr_h;


	// passing structs by val, ptr, ref?
	// returning structs?
	// array of structs?
	// probably all of this will be covered in discussion of classes.
}
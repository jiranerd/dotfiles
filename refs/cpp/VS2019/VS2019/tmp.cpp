




#include <string>
#include <vector>
#include <algorithm> // sort
#include <deque>
#include <unordered_map>
#include <map>
#include <iostream>


using std::string;
using std::cout;
using std::endl;
using std::vector;
using std::deque;
using std::unordered_map;
using std::pair;
using std::make_pair;
using std::map;

//void section(string s) { cout << "\n**********\n* " + s + "\n**********\n\n"; }
void printDequeInt(string s, deque<int> d) { cout << s; for (int i = 0; i < d.size(); i++) { cout << d[i] << ","; } cout << endl; }
bool sort_reverse(int a, int b) { return (a > b); }


// taken from functions.cpp
// todo: replace Foo, Baz with teacup examples so we can track constructors.
class Foo { public: int bar; Foo(int x) :bar{ x } {}; Foo() :bar{ 2 } {}; };
				  //class Baz { public: Foo* foo; int bif; };
class Baz { public: Foo& foo; int bif; };
				  // is this the most idiomatic way to compose classes?
				  // what is the default constructor doing here?

			  //class Baz { public: Foo& foo; int bif;  Baz(Foo& f, int x) { foo = f; bif = x; } };
				  // doesn't work... Foo& can only be initialized in initializer list?

			  //class Baz { public: Foo& foo; int bif;  Baz(Foo& f, int x) :foo{ f }, bif{ x }{} };
				  // this works... why are initializer lists treated specially?
				  // why are they allowed to set reference variables?
				  // probably because reference variables _must_ be initialized with an object,
				  // and the compiler cannot guarantee this will happen in a constructor function body.
				  // https://stackoverflow.com/questions/1701416/initialization-of-reference-member-requires-a-temporary-variable-c

class Box2D
{
public:
	int centerX;
	int centerY;
	int width;
	int height;

	Box2D(int x, int y, int w, int h) :centerX(x), centerY(y), width(w), height(h) {}
	~Box2D() {}

	std::string asString()
	{
		return std::to_string(centerX) + "," + std::to_string(centerY) + "," + std::to_string(width) + "," + std::to_string(height);
	}
};

void foo()
{

	//**********
	//section("vector");
	//**********

	vector<int> v1(3);
	v1[0] = 22;
	v1[1] = 33;
	v1[2] = 44;
	//v1[8] = 55; // succeeds?! but what does it do internally? update: maybe not, only succeeds in Release?
	cout << v1[1] << endl;

	vector<int> v2{ 55, 66, 77 };
	cout << v2[1] << endl;

	// iterate with range-for
	for (auto x : v2) { cout << x << ","; }
	cout << endl;

	// iterate with iterator
	for (auto iter = v2.begin(); iter != v2.end(); iter++) { cout << *iter << ","; }
	cout << endl;

	// iterate and change with iterator
	for (auto iter = v2.begin(); iter != v2.end(); iter++) { (*iter)++; } // careful, ++ has higher precedence than *, so force * first
	cout << endl;

	// iterate and change with range-for
	//for (auto x : v2) { x++; } // doesn't work, only increments copy of element
	for (auto& x : v2) { x++; }
	for (auto x : v2) { cout << x << ","; }
	cout << endl;

	// filter by creating a copy
	vector<int> v3;
	copy_if(v2.begin(), v2.end(), back_inserter(v3), [](int x) { return x < 70; });
	for (auto x : v3) { cout << x << ","; }
	cout << endl;

	// how to filter stl container?
	// https://stackoverflow.com/questions/21204676/modern-way-to-filter-stl-container

	// maybe for now, cleanest way to filter a vector is to iterate and populate a copy vector with good elements.
	// or, use a list?

	// remember, the difficulty which must be overcome when filtering vectors
	// is that c++ requires vectors to be laid out continguously in memory.
	// this means that deletes must shift all remaining elements.


	//**********
	//section("algorithm lib functions on vectors");
	//**********

	sort(v1.begin(), v1.end());
	bool present = binary_search(v1.begin(), v1.end(), 33);
	cout << 33 << " present: " << present << endl;

	// reverse sort
	sort(v1.begin(), v1.end(), sort_reverse);
	for (auto x : v1) { cout << x << ","; }
	cout << endl;

	// lower_bound
	// upper_bound

	// todo: sort a vector of objects/structs

	//**********
	//section("list");
	//**********

	//**********
	//section("strings");
	//**********



	//**********
	//section("queue");
	//**********

	deque<int> dq1{ 4, 5 };
	dq1.push_front(3);
	dq1.push_back(6);
	cout << dq1.front() << "," << dq1.back() << endl;
	dq1.pop_front();
	dq1.pop_back();

	printDequeInt("dq1 = ", dq1);


	//**********
	//section("hashmap"); // in c++, hashmap is available as an unordered_map
	//**********

	unordered_map<int, string> um1{
		{1, "foo"},
		{2, "bar"},
		{10, "bap"},
		{11, "bop"},
		{12, "hop"},
		{13, "hip"}
	};
	cout << um1[2] << endl;

	// insert
	um1[3] = "baz";

	// alternate insert
	um1.insert(make_pair(4, "bif"));

	//
	um1.insert(pair<int, string>(15, "bit"));


	// delete by key
	um1.erase(4);
	um1.erase(5); // does not exist

	// delete by iterator
	um1.erase(um1.begin());
	um1.erase(um1.find(12));

	// contains key
	if (um1.find(10) != um1.end()) { cout << "found key " << 10 << endl; }
	else { cout << "notfound key " << 10 << endl; }
	if (um1.find(20) != um1.end()) { cout << "found key " << 20 << endl; }
	else { cout << "notfound key " << 20 << endl; }

	// iterate with iterator
	for (auto iter = um1.begin(); iter != um1.end(); iter++) { cout << iter->first << ":" << iter->second << " "; }
	cout << endl;

	// iterate by reference
	for (auto& x : um1) { std::cout << x.first << ": " << x.second << std::endl; } // iterating by reference allows me to change val?


	// filter (must use iterator)
	for (auto iter = um1.begin(); iter != um1.end();) // CAUTION: do not do ++ here, you will skip an elt after delete.
	{
		if (iter->second[1] == 'a') { iter = um1.erase(iter); }
		else { iter++; }
	}
	cout << "after filter:" << endl;
	for (auto& x : um1) { std::cout << x.first << ":" << x.second << " "; }
	cout << endl;


	//**********
	//section("tree"); // (can be map (key-value) or set (key))
	//**********

	map<int, string> map1{
		{1, "foo"},
		{2, "bar"}
	};

	// insert
	map1[3] = "baz";
	map1.insert(make_pair(8, "bap"));

	// delete
	map1.erase(8);

	// iterate

	for (auto iter = map1.begin(); iter != map1.end(); iter++) { cout << iter->first << ":" << iter->second << " "; }
	cout << endl;

	// iterate with update

	// iterate with delete (filter)
	// carefully consider 2 approaches given by first answer here: https://stackoverflow.com/questions/596162/can-you-remove-elements-from-a-stdlist-while-iterating-through-it

	//**********
	//section("classes"); // and inheritance
	//**********

	// Box2D defined in header

	// managing vectors of classes
	vector<Box2D> vBox2D1{
		Box2D(1, 2, 3, 4),
		Box2D(2, 3, 4, 5),
		Box2D(3, 4, 5, 6)
	};

	//vector<Box2D>::iterator box2DIter;
	for (auto box2DIter = vBox2D1.begin(); box2DIter != vBox2D1.end(); box2DIter++)
	{
		// CAUTION: this does not update the object.
		//Box2D curr = *box2DIter; // creates a copy
		//curr.centerX++;

		box2DIter->centerX++;
	}
	cout << vBox2D1[0].asString() << endl;

	// iterate using references?
	//for (auto& curr = vBox2D1.begin(); curr != vBox2D1.end(); curr++) // doesn't work
	//{
	//	//
	//}

	// one disappointing feature: it is possible to loop over vectors/maps/deques/etc with range for loop,
	// but there is no way to do deletes with a range for loop.
	// https://stackoverflow.com/questions/10360461/removing-item-from-vector-while-in-c11-range-for-loop
	// for this reason, i think i prefer iterator loops, even though you can't use them to access objects by reference.

	// hm, but if i need deletes then i shouldn't be using vectors anyways?
	// vectors must be contiguous, so i assume delete shifts all remaining items?
	// or use copy_if? http://www.cplusplus.com/reference/algorithm/copy_if/
	// also investigate remove_copy_if: https://en.cppreference.com/w/cpp/algorithm/remove
	// remove_if?

}
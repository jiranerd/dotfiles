


// in c++, every expression has a data type 
// (int, double, int pointer, int lvalue reference, int rvalue reference, etc.) 
// and a value category (lvalue or rvalue).

// todo: explain how data type can be broken down into two components:
// the non-ref component (int, float, complex_num, etc.) 
// and the ref component (value, ptr, lval ref, rval ref).
// for example, int* has non-ref component int and ref component pointer.
// int has non-ref component int and ref component value.
// the ref component determines how the variable interacts with functions.

// an lvalue is an expression which is addressable. (i.e., persistent/named)
// most lvalues can be overwritten. (const lvalues cannot.)
// most lvalues can be converted to rvalues. (arrays, functions, etc. cannot)

// an rvalue is an expression which is not an lvalue. (i.e., temp/anonymous)
// most rvalues cannot be overwritten. (e.g., 3)
// the only way to extend the life of an rvalue and assign to it
// is through an rvalue reference.

// lvalue <=> persistent <=> named <=> variable
// rvalue <=> temporary <=> anonymous <=> literal, arithmetic expression, etc.

// int x = 3 + 2;
	// the expression x is a variable with value category lvalue.
// int x = 3 + 2;
	// the expressions 3 and 2 are literals with value category rvalue.
	// the expression 3 + 2 is an arithmetic expression w value category rvalue.
// int x = y;
	// expressions x and y are lvalues. y is converted to an rvalue.

// int x = 3; int&y = x;
	// expression y has value category lvalue, data type int lvalue reference.

// int&& z = 2 + 3;
	// expression x has value category lvalue, data type int rvalue reference.


// it is possible to be more precise (or more general) than lvalue and rvalue.
// https://en.cppreference.com/w/cpp/language/value_category
// "Each C++ expression (an operator with its operands, a literal, 
// a variable name, etc.) is characterized by two independent properties: 
// a type and a value category."

// rvalue: a prvalue or xvalue.
// prvalue: pure rvalue. 

// glvalue: an lvalue or xvalue.
// lvalue: "pure" lvalue

// xvalue: expiring value. 
	// object or bitfield whose resources can be reused (overwritten)
	// if xvalue has no rvalue reference to it, for practical purposes it is an rvalue.
	// if xvalue has an rvalue reference to it, for practical purposes it is an lvalue.

// https://eli.thegreenplace.net/2011/12/15/understanding-lvalues-and-rvalues-in-c-and-c/
// non-constant lvalue references cannot be assigned rvalues.
// constant lvalue references can be assigned rvalues.

// http://thbecker.net/articles/rvalue_references/section_01.html
// lvalue is an expression that refers to a memory location,
// and lets us get the address with &.
// you cannot take the address of rvalues.

// todo: modifiers static (global lifetime), const (can't be modified), 
// extern (declared elsewhere), extern "C" (function was compiled in C).

// https://en.cppreference.com/w/cpp/language/initialization
// there are 6 types of initialization:
// value, direct, copy, list, aggregate, reference.
// (cppreference also discusses static, constant, zero initalization)
// can also be categorized zero, value, default?
	// https://stackoverflow.com/a/1613578/2512141

// http://thbecker.net/articles/rvalue_references/section_01.html
// "An lvalue is an expression that refers to a memory location 
// and allows us to take the address of that memory location via the & operator. 
// An rvalue is an expression that is not an lvalue."

//foo(x); // argument is lvalue: calls foo(X&)
//foo(foobar()); // argument is rvalue: calls foo(X&&) // how is this not a function definition?



#include <iostream>
#include <limits>
#include <bitset>
#include <string>

using std::cout;
using std::endl;
using std::string;
using std::to_string;
using std::numeric_limits;

void variables()
{
	// data types (non-ref component)
	int a = 1;

	signed int b = 2;
	unsigned int c = 3;

	short int d = 4;
	long int e = 5;
	long long int f = 6;

	long f2 = 7;
	long long f3 = 8;

	float g = 9.0f;
	double h = 10.0;
		// .0 creates a double.

	bool bt = true; 
	bool bf = false;
		// true and false are keywords.
	char j = 'c';

	int null = NULL; 
		// 0, type int
	int* nptr = nullptr; 
		// 0, type int* (but is automatically cast to other pointer types?)
		// also, pass nullptr to call void func(int* x) 
		// rather than void func(int x)?
		// https://stackoverflow.com/questions/20509734/null-vs-nullptr-why-was-it-replaced

	// limits
	cout << "min int: " << numeric_limits<int>::min() << endl;
	cout << "max int: " << numeric_limits<int>::max() << endl;

	cout << "min unsigned int: " << numeric_limits<unsigned int>::min() << endl;
	cout << "max unsigned int: " << numeric_limits<unsigned int>::max() << endl;

	cout << "min long int: " << numeric_limits<long int>::min() << endl;
	cout << "max long int: " << numeric_limits<long int>::max() << endl;

	cout << "min double: " << numeric_limits<double>::min() << endl;
	cout << "max double: " << numeric_limits<double>::max() << endl;

	cout << "size of bool: " << sizeof(bool) << endl;
	cout << "size of char: " << sizeof(char) << endl;

	// different ways to initialize
	int init1 = 1; // copy
	int init2(2);  // direct. why isn't this a function prototype?
	int init3{ 3 }; // uniform. only works at initialization, can't be used for reassignment?

	// also called c-like, constructor, and uniform
	// http://www.cplusplus.com/doc/tutorial/variables/

	//////////
	// consider moving the following initialization discussion to classes.cpp.
	//////////

	// list initialization prevents narrowing conversions (e.g. long to int)
	// note: a narrowing conversion is a type of implicit conversion.
	// for non-class types, ther's no difference between copy and direct init.
	// the only behavior difference is between non-list and list init.
	// however, for classes, direct init doesn't create intermediate temporaries.
	// (and won't invoke copy constructor?)
	// https://stackoverflow.com/questions/21150067/initialization-difference-with-or-without-curly-braces-in-c11
	// https://stackoverflow.com/questions/27352021/c11-member-initializer-list-vs-in-class-initializer

	// T x = a;	  // copy non-list initialization
	// T x = {a}; // copy list initialization

	// T x(a);	  // direct non-list initialization? or have we declared a function prototype here? maybe not, we passed an arg?
		// ah, no, function prototype would look like this: T x(A(foo)); (function x accepts A named foo and returns type T)
	// T x{a};	  // direct list initialization

	// https://stackoverflow.com/questions/8106016/c-default-initialization-and-value-initialization-which-is-which-which-is-ca
	// https://stackoverflow.com/questions/9490349/instantiate-class-with-or-without-parentheses
	// https://docs.microsoft.com/en-us/cpp/cpp/initializing-classes-and-structs-without-constructors-cpp?view=vs-2019

	// T x;      // uninitialized object x?
	// T x();    // declares a function prototype x() which returns T? yup. https://www.fluentcpp.com/2018/01/30/most-vexing-parse/
	// T x{};    // default initialized object x?

	// https://stackoverflow.com/questions/620137/do-the-parentheses-after-the-type-name-make-a-difference-with-new?noredirect=1&lq=1

	// https://stackoverflow.com/questions/5211090/not-using-parentheses-in-constructor-call-with-new-c?noredirect=1&lq=1
	// Class* pC = new Class;
	// Class* pC = new Class();
	// "If the class has a default constructor defined, then both are equivalent; the object will be created by calling that constructor.
	// If the class only has an implicit default constructor, then there is a difference.The first will leave any members of POD type uninitialised; 
	// the second will value - initialise them(i.e.set them to zero)."

	// https://stackoverflow.com/questions/49802012/different-ways-of-initializing-an-object-in-c/49802943
	//class Entity { public: int x, y; Entity() : x(0), y(0) {}; Entity(int x, int y) : x(x), y(y) {}; }

	//Entity ent1;
	// This statement uses default constructor of class `Entity`.

	//Entity ent2();
	// This definition will be treated by compiler as a function prototype if that's possible.

	//Entity ent3(1, 2);
	// User - defined constructor is invoked for `ent3`.

	//Entity ent4 = Entity();
	// This is a*** proper*** version of `ent2` case.Default constructor is invoked as part of value initialization.Its form allows to avoid Most Vexing Parse - ambiguity solving principle which makes `ent2` incorrect.

	//Entity ent5 = Entity(2, 3);
	// A version of ent3 case.User - defined constructor invoked as part of value initialization.

	//Your question is tagged as C++11, and C++11 allows uniform initialization syntax :

	//Entity ent12{};     // This is a legal alternative of ent2 case
	//Entity ent13{ 1, 2 };
	//Entity ent14 = {};
	//Entity ent15 = Entity{ 2, 3 };

	// Note that uniform initialization syntax has a caveat.E.g. this line
	//std::vector<int> v(10);
	// declares a vector of 10 elements.But this one
	//std::vector<int> v{ 10 };
	// declares a vector initialized with single element of type int with value 10. This happens because `std::vector` has a constructor with following signature defined :
	//vector(std::initializer_list<T> init, const Allocator & alloc = Allocator());
	// In case that you can't use neither () without triggering MVP nor {} without invoking undesired constructor, the value initialization assignment syntax allows to resolve the issue.

	//////////
	// initialization discussion continued...
	//////////

	// https://stackoverflow.com/questions/1051379/is-there-a-difference-between-copy-initialization-and-direct-initialization
	// CAVEAT: example below changed in C++17?

	//void my_test()
	//{
	//	A a1 = A_factory_func();
	//	A a2(A_factory_func());
		// Depends on what type A_factory_func() returns. I assume it returns an A - then it's doing the same - 
		// except that when the copy constructor is explicit, then the first one will fail. Read 8.6/14

	//	double b1 = 0.5;
	//	double b2(0.5);
		// This is doing the same because it's a built-in type (this means not a class type here). Read 8.6/14.

	//	A c1;
	//	A c2 = A();
	//	A c3(A());
		// This is not doing the same. The first default-initializes if A is a non-POD, and doesn't do any initialization for a POD (Read 8.6/9). 
		// The second copy initializes: Value-initializes a temporary and then copies that value into c2 (Read 5.2.3/2 and 8.6/14). 
		// This of course will require a non-explicit copy constructor (Read 8.6/14 and 12.3.1/3 and 13.3.1.3/1 ). 
		// The third creates a function declaration for a function c3 that returns an A and that takes a function pointer to a function returning a A (Read 8.2).
	//}

	// copy vs direct initialization:
	//T t = x;
	//T t(x);

	//////////
	// initialization discussion end
	//////////


	// casting (explicit conversion)
	double doub = 18.0;
	short shor = short(doub);
		// no compiler warning?

	// alternative casting syntax
	short shor2 = (short)doub;

	// coercion (implicit conversion)
	cout << "3 / 2 = " << 3 / 2 << endl;
	cout << "3 / 2.0 = " << 3 / 2.0 << endl;
	cout << "3.0 / 2 = " << 3.0 / 2 << endl;

	// coercion can also happen when mixing doubles and floats,
	// unsigned and signed, etc.

	unsigned int ui = 4;
	int si = -8;
	cout << (si < 0) << endl;
		// outputs 1, as expected.
	cout << ((ui + si) < 0) << endl;
		// CAUTION!! outputs 0. ui + si is treated as unsigned int.
	cout << ui + si << endl;
		// outputs 4 294 967 294, cout treats ui + si as unsigned.

	// int, float to string
	string str_int = to_string(3);
	string str_flt = to_string(4.0f);

	cout << str_int << "," << str_flt << endl;
	cout << 3 << "," << 4.0f << endl;
		// note: to_string() converts to string differently than <<.

	// precision
	// https://floating-point-gui.de/
	double one_tenth = 1.0 / 10.0;
	std::streamsize strsz_orig = cout.precision();
		// save this so we can restore it later.
	cout.precision(std::numeric_limits<double>::max_digits10);
		// max_digits10 = 17. 
		// it is the number of decimal digits required to avoid loss of accuracy
		// when converting a double from binary to a string and back.
		// http://www2.open-std.org/JTC1/SC22/WG21/docs/papers/2005/n1822.pdf
		// https://stackoverflow.com/q/554063/2512141
	cout << "1.0 / 10.0 = " << one_tenth << endl;
	cout.precision(strsz_orig);
		// restore precision.
	cout << one_tenth << endl;

	// hex and binary literals.
	int nine = 0b1001; 
		// supported in C++14
	int twenty_two = 0x16;
	std::cout << nine + twenty_two << std::endl;

	// printing as hex, binary
	cout << std::bitset<32>(nine) << endl;
	cout << std::bitset<32>(twenty_two) << endl;

	// unsigned literals
	unsigned long long int ll_int = 18446744073709551613ull;
		// not sure yet why this is needed...

	// scientific notation
	double dbl_x = 314.159e-2;


	// data types (ref component)
	// variable, pointer, lvalue reference, rvalue reference
	int val = 3;
	int* val_ptr = &val;
	int& lval_ref = val;
		// a reference is a pointer which the compiler automatically dereferences.
		// references must be initialized when created.
		// you can only initialize them with non-constant lvalues.
		// lvalue references cannot be reassigned.
	//int&& rval_ref = val;
		// not allowed. val is an lvalue.
	int&& rval_ref = std::move(val);
		// allowed.
		// also, rvalue references can be reassigned.

	val = 4;
	cout << val << endl;
	*val_ptr = 5;
	cout << val << endl;
	lval_ref = 6;
	cout << val << endl;
	rval_ref = 7;
	cout << val << endl;


	int&& rval_ref_2 = 3;
	rval_ref_2 = 4;
	cout << rval_ref_2 << endl;
		// one way to think of an rvalue ref is as a reference to an unnamed (or anonymous) value, whereas an lvalue ref is a reference to a named value.
		// rvalue refs are most often seen as the arguments to class move constructor and move assignment functions,
		// or when writing custom class operators (e.g., for adding two classes)

		// note: rval_ref_2 has type "int rvalue reference", but it is itself an lvalue.
		// so we have an lvalue with type "int rvalue reference"
		// https://www.learncpp.com/cpp-tutorial/15-2-rvalue-references/
		// "Although variable ref has type r-value reference to an integer, it is actually an l-value itself (as are all named variables). ...
		// Named-objects are l-values. Anonymous objects are r-values. 
		// The type of the named object or anonymous object is independent from whether it�s an l-value or r-value."


	// constant values must be initialized with a value.
	const int always_one = 1;
	//always_one = 2;
		// error: expression must be a modifiable l-value

	// auto, decltype
	int x_aa = 88;
	auto auto_type = x_aa;
	decltype(x_aa) decltype_type = x_aa;
	// decltype is used to make typedefs in templates?
	// http://thbecker.net/articles/auto_and_decltype/section_05.html

}


#include <iostream>
#include <vector>
#include <string>

using std::cout;
using std::endl;
using std::vector;
using std::string;
using std::to_string;

// best practice for returning a vector in c++:
// https://stackoverflow.com/questions/4986673/c11-rvalues-and-move-semantics-confusion-return-statement?rq=1

class ComplexNumVerbose
{
public:
	int real;
	int imag;

	ComplexNumVerbose(int r, int i) :real(r), imag(i) { cout << "constructor " << r << "," << i << endl; };
	~ComplexNumVerbose() { cout << "destructor " << real << "," << imag << endl; }
	string to_string() { return std::to_string(real) + "+" + std::to_string(imag) + "i"; }

	ComplexNumVerbose(const ComplexNumVerbose& other) { 
		cout << "copy constructor from:" << other.real << "," << other.imag << endl; 
		real = other.real;
		imag = other.imag;
	}

	ComplexNumVerbose& operator= (const ComplexNumVerbose& other) {
		cout << "copy assignment from:" << other.real << "," << other.imag << " to:" << real << "," << imag << endl; 
	}

	ComplexNumVerbose(ComplexNumVerbose&& other) noexcept {
		cout << "move constructor from:" << other.real << "," << other.imag << endl;
		real = other.real;
		imag = other.imag;
		other.real = -1;
		other.imag = -1;
	}

	ComplexNumVerbose& operator= (ComplexNumVerbose&& other) noexcept {
		cout << "move assignment from:" << other.real << "," << other.imag << " to:" << real << "," << imag << endl;
		real = other.real;
		imag = other.imag;
		other.real = -1;
		other.imag = -1;
	}
};

void vector_of_objects()
{

	// vector of objects
	vector <ComplexNumVerbose> v_cplx;
	for (int i = 0; i < 3; i++)
	{
		v_cplx.push_back(ComplexNumVerbose(i, int(2 * i)));
		// are these allocated on the stack?
		// no, they can't be allocated on the stack bc the number of iterations is not known at compile time.
		// so what does ComplexNumVerbose() return? push_back() tooltip shows ComplexNumVerbose &&_Val.

		// don't do this! adding a destructor showed me that objects are copied/destroyed with every resize 
		// (caused by call to push_back())

		// finally found good SO post on vector of objects vs vector of ptrs.
		// https://stackoverflow.com/a/2694231/2512141

		// what does calling the constructor without "new" do?
		// answer? https://stackoverflow.com/questions/2722879/calling-constructors-in-c-without-new
		// 1) Create an object of type Thing using the constructor Thing(const char*)
		// 2) Create an object of type Thing using the constructor Thing(const Thing&)
		// 3) Call ~Thing() on the object created in step #1

		// however, one thing that this approach does correctly, is that vector<> will call destructors on its contained objects.
		// is this because ComplexNumVerbose() returns an rvalue reference?

		// ah, vector will use move if the move constructor is declared noexcept?
		// https://stackoverflow.com/questions/8001823/how-to-enforce-move-semantics-when-a-vector-grows
		// yup!

	}
	cout << v_cplx[1].to_string() << endl;




	// vector of objects, attempt 2
	vector <ComplexNumVerbose*> v_cplx_ptr;
	for (int i = 0; i < 3; i++)
	{
		v_cplx_ptr.push_back(new ComplexNumVerbose(10 + i, 10 + int(2 * i)));
		// these are not destructed.
		// vector<> doesn't know what kind of pointers it has stored.
		// https://stackoverflow.com/questions/9448260/does-stdvector-call-the-destructor-of-pointers-to-objects
	}
	
	cout << v_cplx_ptr[1]->to_string() << endl;

	// vector of objects, attempt 3
	vector <std::unique_ptr<ComplexNumVerbose>> v_cplx_uptr;
	for (int i = 0; i < 3; i++)
	{
		v_cplx_uptr.push_back(
			std::unique_ptr<ComplexNumVerbose>(new ComplexNumVerbose(20 + i, 20 + int(i * 2)))
		);
	}
	cout << v_cplx_uptr[2].get()->to_string() << endl;




	// vector of arrays

	// n-dimensional vectors
	// yuck...
	// https://stackoverflow.com/questions/13105514/n-dimensional-vector

	// 2d vectors

	// how to sort a vector of objects
	// separate example for sorting structs?

}
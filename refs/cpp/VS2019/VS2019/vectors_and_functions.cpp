


// 4 examples returning vector from a function (4th is best practice)
// not sure if 4th example is correct to call variable "rval_ref", since it has no ampersand...
// https://stackoverflow.com/questions/4986673/c11-rvalues-and-move-semantics-confusion-return-statement

//std::vector<int> return_vector(void)
//{
//	std::vector<int> tmp{ 1,2,3,4,5 };
//	return tmp;
//}
//std::vector<int>&& rval_ref = return_vector();
//
//std::vector<int>&& return_vector(void)
//{
//	std::vector<int> tmp{ 1,2,3,4,5 };
//	return std::move(tmp);
//}
//std::vector<int>&& rval_ref = return_vector();
//
//std::vector<int> return_vector(void)
//{
//	std::vector<int> tmp{ 1,2,3,4,5 };
//	return std::move(tmp);
//}
//std::vector<int>&& rval_ref = return_vector();
//
// Best Practice.
//std::vector<int> return_vector(void)
//{
//	std::vector<int> tmp{ 1,2,3,4,5 };
//	return tmp;
//}
//std::vector<int> rval_ref = return_vector();

// https://stackoverflow.com/questions/15704565/efficient-way-to-return-a-stdvector-in-c
// after C++11, returning a vector by value is preferred.
// with C++11, std::vector has move semantics.
//std::vector f()
//{
//	std::vector<int> result;
//	return result;
//}

// Herb Sutter says we no longer need to pass const references?
// https://stackoverflow.com/questions/10231349/are-the-days-of-passing-const-stdstring-as-a-parameter-over?rq=1

// series, RValue References: Moving Forward (2009)
// https://web.archive.org/web/20140113221447/http://cpp-next.com/archive/2009/08/want-speed-pass-by-value/
// "Rvalues are expressions that create anonymous temporary objects."
// "Once you know you are copying from an rvalue, then, it should be possible
// to 'steal' the expensive-to-copy resources from the source object 
// and use them in the target object without anyone noticing."


#include <iostream>
#include <vector>
#include <algorithm>

using std::cout;
using std::endl;
using std::vector;
using std::sort;
using std::greater;

//int vec_sum(vector<int> v)
	// this works, but it creates a copy of the vector.
	// https://www.geeksforgeeks.org/passing-vector-function-cpp/
	// i wonder if the entire vector is placed on the calling stack?
int vec_sum(vector<int>& v)
	// this is the preferred way to pass a vector.
	// https://stackoverflow.com/questions/4061128/how-to-pass-2-d-vector-to-a-function-in-c
{
	int sum = 0;
	for (auto x : v)
	{
		sum += x;
	}
	return sum;
}

int vec_sum_ptr(vector<int>* ptr_v)
{
	int sum = 0;
	for (auto x : *ptr_v)
	{
		sum += x;
	}
	return sum;
}

// https://stackoverflow.com/questions/15704565/efficient-way-to-return-a-stdvector-in-c
// "std::vector has move-semantics, which means the local vector declared in your function
// will be moved on return."
vector<int> odd_vec()
{
	vector<int> ret;
	for (int i = 1; i < 6; i++)
	{
		ret.push_back(i);
	}
	return ret;
}

vector<int> return_vector()
{
	vector<int> tmp{ 1, 2, 3 };
	return tmp;
}

vector<int>& return_vector_lval_ref()
{
	vector<int> tmp{ 1, 2, 3 };
	return tmp;
}

vector<int>&& return_vector_rval_ref()
{
	vector<int> tmp{ 1, 2, 3 };
	return std::move(tmp);
}

void vectors_and_functions()
{

	vector<int>&& vec_j = std::move(return_vector_lval_ref());

	vector<int> v_int{ 1, 2, 3 };

	// pass vector to function
	int vs = vec_sum(v_int);
	cout << "sum: " << vs << endl;

	// pass vector as a pointer
	vs = vec_sum_ptr(&v_int);
	cout << "sum: " << vs << endl;

	// return vector from function
	vector<int> odd = odd_vec();
	for (auto x : odd) { cout << x << ","; }
	cout << endl;

	// todo: pass vector of objects to a function
	// todo: return vector of objects from a function

	// nine permutations of vector return type and assigned variable type.
	// inspired by https://stackoverflow.com/questions/4986673/c11-rvalues-and-move-semantics-confusion-return-statement?rq=1
	vector<int> vec_a = return_vector();
	//vector<int> vec_b = return_vector_lval_ref();
		// std::bad_alloc at memory location 
	//vector<int> vec_c = return_vector_rval_ref();
		// Exception thrown: read access violation.

	//vector<int>& vec_d = return_vector();
		// initial value of reference to non-const must be an lvalue
	vector<int>& vec_e = return_vector_lval_ref();
	//vector<int>& vec_f = return_vector_rval_ref();
		// initial value of reference to non-const must be an lvalue

	vector<int>&& vec_g = return_vector();
	//vector<int>&& vec_h = return_vector_lval_ref();
		// an rvalue reference cannot be bound to an lvalue
	vector<int>&& vec_i = return_vector_rval_ref();


}
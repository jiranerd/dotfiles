
#include "single.hpp"

int main(int argc, char* argv[])
{
	cout << "hello" << endl;

	// Note the symmetry below between int, class, and container.
	// Note that array has slightly different constraints.

	// In examples below, main() demonstrates declare, assign, and pass.
	// Functions demonstrate return.
	// For example, return_int_stack() demonstrates how to return an int
	// that lives on the function (not caller) stack.

	// Int on stack. Declare, assign, pass, return.

	int int_stack;
	int_stack = 3;
	pass_int_stack(int_stack);
	int int_stack_ret = return_int_stack();

	// Int on heap. Declare, assign, pass, return.

	int* int_heap = new int;
	*int_heap = 6;
	pass_int_heap(int_heap);
	delete int_heap;
	int* ptr_int_ret = return_int_heap();
	delete ptr_int_ret;

	// Class/struct on stack. Declare, assign, pass, return.

	point_2d class_stack;
	class_stack.x = 3;
	class_stack.y = 4;
	pass_class_stack(class_stack);
	point_2d class_stack_ret = return_class_stack();

	// Class/struct on heap. Declare, assign, pass, return.

	point_2d* class_heap = new point_2d;
	class_heap->x = 5;
	class_heap->y = 6;
	pass_class_heap(class_heap);
	delete class_heap;
	point_2d* class_heap_ret = return_class_heap();
	delete class_heap_ret;

	// Container on stack. Declare, assign, pass, return.
	vector<int> container_stack;
	container_stack.push_back(5);
	pass_container_stack(container_stack);
	//cout << container_stack[1] << endl; // Throws an error. Function modified its copy, not our copy.
	vector<int> container_stack_ret = return_container_stack();

	// Container on heap. Declare, assign, pass, return.
	vector<int>* container_heap = new vector<int>;
	container_heap->push_back(17);
	pass_container_heap(container_heap);
	delete container_heap;
	vector<int>* container_heap_ret = return_container_heap();
	delete container_heap_ret;

	// Array on stack. Declare, assign, pass, return.

	int arr_stack[4];
	arr_stack[0] = 7;
	pass_arr_stack(arr_stack);
	// No way to receive an array returned from function's stack
	// because we don't know at compile time how large returned array would be.

	// Array on heap. Declare, assign, pass, return.

	int* arr_heap = new int[4];
	arr_heap[0] = 8;
	pass_arr_heap(arr_heap);
	delete[] arr_heap;
	int* arr_int_ret = return_arr_heap();
	delete[] arr_int_ret;

	// Composing containers.

	// Composing classes. (subclasses live on stack or on heap?)

	// Using unique_ptr to improve memory safety for pointers.
	// Using unique_ptr to improve memory safety for arrays.

	// Passing stack int, stack struct, stack container by pointer.
	// (Use & to get address.)

	// Passing stack int, stack struct, stack container by reference.
	// Passing by reference allows the function to modify a val
	// living in caller's stack.
	// Unfortunately, from caller's code it looks the same as pass by copy,
	// so you cannot tell simply from function call whether it will modify your value.


	// Composing classes on the stack.

	square_2d square;
	point_2d a;
	a.x = 4;
	a.y = 5;
	point_2d b;
	b.x = 8;
		// Interesting... If we assign nothing to b, compiler complains about uninitialized.
		// But assigning to only one of its members removes the error...
	square.top_left = a;
	square.bot_right = b;
	cout << square.top_left.x << endl;

	// Alternative notation. Avoids copies?
	square_2d square_2{ point_2d{1, 2}, point_2d{3, 4} };
	cout << square_2.top_left.x << endl;

	// CAUTION. Function declaration for a function called square_3()
	// which returns a square_2d and expects pointers to two functions which return point_2d.
	// https://stackoverflow.com/questions/1051379/is-there-a-difference-between-copy-initialization-and-direct-initialization
	square_2d square_3(point_2d(), point_2d());
	//cout << square_3.top_left.x << endl;
		// error: square_2d square_3(point_2d(*)(), point_2d(*)());

	// Composing classes on the heap.

	// Class. Explicit *tors.
	{
		scalar s1;
		scalar s2 = s1;
		scalar s3(); // function definition!!

		scalar s4 = scalar();
	}

	// Two meanings for keyword "static" depending on context:
	// 1) variable in a function, variable in a class.
	// 2) class object, function in a class.
	// https://www.geeksforgeeks.org/static-keyword-cpp/

	// Example of RVO?
	vector<int> rvo = return_fours(8);
	cout << rvo.size() << endl;

	// Example of using a returned lvalue reference to populate a custom data structure.


	// Lvalues and lvalue references.

	// An lvalue is a location in memory. Exactly like an address.
	// If you return an lvalue from a function, you have returned a memory location.
	// Just as a pointer stores an address, an lvalue reference stores an lvalue.
	// The primary differences between a pointer and an lvalue reference are that 
	// 1) the lvalue ref is constant,
	// 2) the lvalue ref must be initialized at declaration (because it is constant), 
	// and 3) the lvalue ref is automatically dereferenced, 
	// so dereference and address operators (*,&) are not needed.
	// One case where lvalues are useful is if you write a custom class and wish to define
	// a bracket [] operator to return a location within your class which can be assigned to.

	// NOTE: My definition here is different from a popular alternative I have seen:
	// https://softwareengineering.stackexchange.com/a/299439
	// "lvalue: a value that resides in memory (heap or stack) and is addressable"
	// I disagree with this definition. Functions returning lvalues clearly return a location which can be assigned to, not a value. (??)

	int w = 90;
	int* ptr_w = &w;
	int& lref_w = w;
	int* ptr_lref_w = &lref_w;
	// +		ptr_w	0x00000014466ff494 {3}	int *
	// +		ptr_lref_w	0x00000014466ff494 {3}	int *
	
	int& lref_ret = return_lref(w);
	lref_ret = 91;
	cout << w << endl;

	return_lref(w) = 92;
	cout << w << endl;

	// Rvalues and rvalue references.

	// An rvalue is a value that has not been stored in a variable.
	// For example:
	int x = 3 + 4;
	// Here, 3 and 4 are not stored in a variable. They are rvalues.
	// The computed sum 7 is also an rvalue. The copy of 7 which is put into location x is not an rvalue.

	// Rvalue references can bind to rvalues, but not lvalues.
	int&& rval_ref = 3 + 4; // ok.
	//int&& rval_ref_x = x; // error: an rvalue reference cannot be bound to an lvalue.
	int&& rval_ref_move = std::move(x); // ok. std::move() casts x to an rvalue.
	rval_ref_move = 8;
	cout << x << endl;


	// We can preserve 7's status as an rvalue by storing it in an rvalue reference.
	// It looks the same as a regular variable, but no memory has been allocated for rval_ref! (??)
	// Not sure about this...
	//int&& rval_ref = 3 + 4;
	//cout << rval_ref << endl;

	// An rvalue reference can only bind to rvalues.
	int xx = 8;
	//int&& rval_ref_xx = xx; // error: rvalue ref can only bind to an lvalue.
	int&& rval_ref_8 = 8; // ok

	// You cannot get an address for an rvalue. It has none!
	// https://stackoverflow.com/questions/34221287/how-are-rvalues-in-c-stored-in-memory
	//int&& rval_ref_9 = 9;
	//int* ptr_rval = &rval_ref_9; // works...
	// Um...
	//int* pxp = &(1 + 3); error.

	// Why is this useful?
	// First, we have to know that we can trigger different overloaded functions based on whether we pass an lvalue or rvalue.
	// Here we will learn something very nuanced about C++. Function calls match function declarations based not on the types of the argument _variables_,
	// but rather based on the types of the argument _expressions_. We have not noticed this before because in most cases, the variable type and expression type are the same.
	int val = 7;
	cout << baz(val) << endl;					// 8
	cout << baz(7) << endl;						// 9
	cout << baz(rval_ref) << endl;				// Surprise! Returns 8 because even though rval_ref has type rvalue reference, the expression "rval_ref" is an lvalue.
	cout << baz(std::move(val)) << endl;		// 9
	cout << baz(std::move(rval_ref)) << endl;	// 9

	// Suppose that instead of summing integers, we are summing classes. For example:
	// point_3d v1 = v2 + v3;
	// Assume we have defined an operator "+" which combines two point_3d objects and produces a third one.
	// The produced point_3d is an rvalue. To persist it, it must be copied into v1.
	// This is inefficient. However, if we have defined 

	// https://softwareengineering.stackexchange.com/questions/299437/what-are-r-value-references-used-for/299439#299439
	// "A common [rvalue ref] use case is the move constructor. An example is foo = bar + baz;"
	// Is this correct?

	// https://stackoverflow.com/a/18203453/2512141
	// "If you need one single argument why rvalue references are essential, it's unique_ptr."

	// https://www.quora.com/What-is-the-use-of-an-R-value-reference-variable-like-int
	// "Rvalue references are very rarely useful outside of function parameters.
	// I've seen rvalue reference class members being useful, 
	// but I don't think I've ever seen or needed a local rvalue-reference variable."

	// https://www.quora.com/What-are-some-practical-use-cases-of-rvalue-references-in-C++
	// https://www.quora.com/Can-someone-help-a-beginner-understand-the-value-of-rvalue-references-in-C++11

	// Thank you!! "The _variable_ r is an rvalue reference. But the _expression_ r on line 5 is an lvalue."
	// https://medium.com/@barryrevzin/value-categories-in-c-17-f56ae54bccbe
	// Also, in C++17, these are exactly equivalent:
	//T var = T();
	//T var();
	// Sigh, nvm...
	// In C++17, T var = T(); causes no move?

	// https://www.internalpointers.com/post/c-rvalue-references-and-move-semantics-beginners (2018)
	// "On line (1) the literal constant 666 is an rvalue: it has no specific memory address, 
	// except for some temporary register while the program is running. It needs to be stored in a lvalue (x) to be useful."

	// "This wouldn't be possible without rvalue references and its double ampersand notation."
	// Wrong. I tried it. It works:
	std::string s1 = "foo";
	std::string s2 = "bar";
	std::string s3 = s1 + s2;
	s3 += "baz";
	cout << s3 << endl;

	// http://thbecker.net/articles/rvalue_references/section_02.html
	// X x;
	// x = foo();
	// The last line above
	// - clones the resource from the temporary returned by foo,
	// - destructs the resource held by x and replaces it with the clone,
	// - destructs the temporary and thereby releases its resource.
	// (I don't know if this is still valid for C++17.)

	// https://akrzemi1.wordpress.com/2018/05/16/rvalues-redefined/
	// C++17 introduced guaranteed copy elision.
	// "C++ has completely changed the meaning of rvalue (actually, prvalue)."
	// Great...
	// In C++17:
	// X x = f();
	// is equivalent to:
	// X x(0);
	// "There are no temporaries involved. The type does not have to be movable."

	// https://akrzemi1.wordpress.com/2011/11/09/lvalues-rvalues-and-references/
	// rvalues as of 2011.
	// "First thing to note is that lvalues and rvalues, counter to what their names suggest, 
	// are not properties of values � they are properties of expressions."
	// Explains how to exploit lvalue, rvalue function overloads.

	// https://www.codeproject.com/Articles/453022/The-new-Cplusplus-11-rvalue-reference-and-why-you (2012)
	// "References are internally still just pointers but this time made little bit safer and automatically dereferenced."

	// https://medium.com/@kpl.vermani/move-semantics-in-c-11-a0b836a8b4fa (2019)
	// lvalue: anything you can take an address of. x, arr[10], rval_ref_to_3.
	// rvalue: anything that is not an lvalue. 3, generate_object_x(), (8 - 5).

	// What is stored in an lvalue reference? An address. The address of a variable, location in an array, member of an object, etc.
	// What is stored in an rvalue reference? An address. The address of a value/object not bound to a variable. 
		// Unnamed values/objects are safe to change bc no one else will be able to read from them.
		// But doesn't assigning the value/object to the rval ref bind it? Well, yes, in a single scope. But not in a called function scope!
		// If we see an rval ref as a function argument, it means that in the caller's scope, that value/object is bound to no variable,
		// and we can change it as we see fit!

	// What is stored in an lvalue reference? The memory location (address) of an lvalue.
		// Lvalue references are useful in several cases.
		// One case is when we wish to define a bracket "[]" operator for a custom class,
		// and we wish it to return an address that can be assigned to.
		// Another case is when we wish to write a function definition which can access some object in calling scope (as with ptr),
		// but ...
	// What is stored in an rvalue reference? The memory location (address) of an rvalue.
		// Within a single scope, rvalue references are not very useful.
		// After all, if we need to store the memory location of an rvalue, we can use a regular variable.
		// Rvalue references are much more useful as function arguments.
		// An rvalue reference parameter in a function tells us that in the calling scope, that value/object has is not bound to any variable,
		// and therefore we can modify it as we please without consequence.

	int m = 3;

	//int& lval_ref_a = 3; // error: initial value of reference to a non-const must be an lvalue.
	int& lval_ref_a = m; // ok.

	int&& rval_ref_a = 3; // ok.
	//int&& rval_ref_a = m; // error: an rvalue reference cannot be bound to an lvalue.






	// RAII

	// SBRM "Scope-Bound Resource Management", aka RAII "Resource Acquisition is Initialization".
	// https://embeddedartistry.com/blog/2017/07/17/migrating-from-c-to-c-take-advantage-of-raii-sbrm/

	// Optimization: passing const refs.



}
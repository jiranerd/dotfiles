#pragma once

#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

class point_2d
{
public:
	int x;
	int y;
};

class square_2d
{
public:
	point_2d top_left;
	point_2d bot_right;
};

void pass_int_stack(int x) { cout << x << endl; }
int return_int_stack() { return 3; }

void pass_int_heap(int* x) { cout << *x << endl; }
int* return_int_heap() { int* ret = new int; *ret = 3; return ret; }

void pass_class_stack(point_2d p) { cout << p.x << endl; }
point_2d return_class_stack() { point_2d ret; ret.x = 8; ret.y = 9; return ret; }

void pass_class_heap(point_2d* p) { cout << p->x << endl; }
point_2d* return_class_heap() { point_2d* ret = new point_2d; ret->x = 1; ret->y = 2; return ret; }

// CAUTION!! I previously thought that containers (such as vector<>) were simply structs containing pointers to underlying data structures.
// I assumed that methods called on a container passed in by copy would affect the container in caller space.
// This was wrong!! Calling push_back() on the function's copy of the vector does not affect caller's vector.
// So does that mean vectors passed by copy are deep copied, data and all? Interesting... That would have serious performance repercussions.
// Also see https://stackoverflow.com/questions/11348376/std-vector-c-deep-or-shallow-copy.
// "You are making a deep copy any time you copy a vector. But if your vector is a vector of pointers you are getting
// the copy of pointers, not the values pointed to."
// Update: Or, a container actually does contain pointers (and this allows returning vector from function without copying everything?),
// but copy constructor is called if passed to function thoughtlessly.

// Also, note that container subscript operator [] returns a reference, but copying that reference to non-ref will make a copy.
// https://stackoverflow.com/questions/19608535/passing-a-vector-by-reference-to-function-but-change-doesnt-persist

void pass_container_stack(vector<int> v) { cout << v[0] << endl; v.push_back(44); }
vector<int> return_container_stack() { vector<int> ret; ret.push_back(13); return ret; }

void pass_container_heap(vector<int>* v) { cout << (*v)[0] << endl; }
vector<int>* return_container_heap() { vector<int>* ret = new vector<int>; ret->push_back(21); return ret; }

void pass_arr_stack(int* x) { cout << x[0] << endl; }

void pass_arr_heap(int* x) { cout << x[0] << endl; x[1] = 7; }
int* return_arr_heap() { int* ret = new int[4]; ret[0] = 9; return ret; }

int& return_lref(int& x) { return x; }


// Better to move this inside scalar{} class?
//static int num_scalars;

class scalar { 
public: 

	// assignment (copy?) operator should return *this?
	// https://www.internalpointers.com/post/c-rvalue-references-and-move-semantics-beginners

	// This cannot be set inside any constructor. It must be set outside the class.
	static int num_scalars;

	// Const members can only be set in initializer list.
	const int id;

	// Add some member that supports move semantics so we can demonstrate?
	// Or maybe instead add a "parent" class which will contain scalar objects, then define moves for that class?
	int x; 

	//scalar(int arg_x){ id = num_scalars++; x = arg_x; // error
	//scalar(int arg_x):id(num_scalars++) { x = arg_x; } // mixed...
	//scalar(int x) :id(num_scalars++), x(x) {}

	// constructor
	scalar() :id(num_scalars++) { cout << "scalar " << id << " constructor" << endl; }

	// destructor
	~scalar() { cout << "scalar " << id << " destructor" << endl; }

	// copy constructor
	scalar(const scalar& s) :id(num_scalars++), x(s.x) { cout << "scalar " << id << " copy constructor" << endl; }

	// copy assignment
	scalar& operator=(const scalar& s) { x = s.x; cout << "scalar " << id << " copy assignment" << endl; }

	// move constructor (class has no pointer members, so this would normally not be needed?)
	// see https://stackoverflow.com/questions/22048644/move-semantics-for-pod-ish-types, move semantics for POD types.
	scalar(scalar&& s) :id(num_scalars++) { x = s.x; cout << "scalar " << id << "move constructor" << endl; }

	// move assignment (class has no pointer members, so this would normally not be needed?)
	scalar& operator=(scalar&& s) { x = s.x; cout << "scalar " << id << "move assignment" << endl; }
};
int scalar::num_scalars = 0;


// Example of using RVO to return a vector without copying everything?
vector<int> return_fours(int n)
{
	vector<int> ret;
	for (int i = 0; i < n; i++)
	{
		ret.push_back(4);
	}
	return ret;
}

int bax(int x) { return x + 3; } // interesting... baz(7) hits baz(int&&), not this one...
int baz(int& x) { return x + 1; }
int baz(int&& x) { return x + 2; }
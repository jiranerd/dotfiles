import java.util.Scanner;

public class Hello {
    public static void main(String[] args){
        System.out.println("hello world");

        // A few ways to run this program:
        //
        // In intellij, set edit run config to make this the main class then click Run->Run 'Hello'
        // Provide input interactively.
        //
        // $ java Hello
        // Then provide input interactively.
        //
        // $ echo "hi\n3\n" | java Hello
        //
        // $ cat input.txt | java Hello

        if(args.length > 0){
            System.out.println(args[0]);
        }
        else{
            Scanner scanner = new Scanner(System.in);
            String userInput = scanner.next(); // reads from stdin, which can be a pipe or interactive shell
            System.out.println(userInput);
            int userInt = scanner.nextInt();
            System.out.println(userInt);
            scanner.close();
        }

    }
}

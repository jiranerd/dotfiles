package com.foo.bar;

import java.util.*;

public class Goodbye {
    public static void main(String[] args){
        System.out.println("goodbye");


        // inheritance: superclass WheeledVehicle, subclasses Car and Bicycle extend WheeledVehicle,
        // so they inherit its members and methods.
        System.out.println("=== inheritance ===");
        Car c = new Car();
        Bicycle b = new Bicycle();

        System.out.println(c.toString());
        c.turnKey();
        System.out.println(b.toString());
        b.switchKickstand();
        b.switchKickstand();

        // polymorphism: Any object that passes more than one IS-A test is polymorphic.
        // For example, Car IS-A Car and Car IS-A WheeledVehicle.

        // WheeledVehicle var can refer to veh or car (implicit upcast), but Car var cannot refer to veh
        WheeledVehicle wheeledVehicle = new WheeledVehicle(6,2);
        System.out.println(wheeledVehicle.toString()); // numWheels:6 numSeats:2
        wheeledVehicle = new Car(); // Works. Car has all the methods that vehicle needs.
        System.out.println(wheeledVehicle.toString()); // Car: numWheels:4 numSeats:4

        Car car = new Car();
        //car = new WheeledVehicle(8, 10); // Incompatible types! wheeled vehicle won't have car functions.

        // upcasting: Cast a subclass up to a superclass. Can be explicit or implicit.
        WheeledVehicle wheeledVehicle2 = new Car();
        //WheeledVehicle wheeledVehicle2 = (WheeledVehicle)(new Car()); // also works
        if(wheeledVehicle2 instanceof Car){ System.out.println("Yes, wheeledVehicle is pointed to an instance of Car."); }
        // NOTE: instanceof checks the object POINTED AT, not the variable type.

        // downcasting: Cast a superclass down to a subclass. Must be explicit.
        //Car car2 = wheeledVehicle2; // Incompatible types, even though wheeledVehicle is pointed to a Car.
        Car car2 = (Car)wheeledVehicle2;
            // This compiles because compiler knows wheeledVehicle could be pointed at a Car.
            // If wheeledVehicle is not pointed at a Car, there will be a runtime exception.
        car2.turnKey();

        // another use for downcasting: superclass variable is pointed to subclass object, wants to use a method that is only in subclass
        ((Car) wheeledVehicle2).turnKey();

        // virtual methods (Bad section. There is no "virtual" keyword in java. In OOP in general, a virtual method is a method overridden in a subclass.)
        // https://www.jitendrazaa.com/blog/java/virtual-function-in-java/


        // abstract class with an abstract method: types of doors
        System.out.println("=== abstract classes ===");
        //Door d = new Door(); // door is abstract, cannot be instantiated! (doesn't matter if it has abstract method or not)
        RevolvingDoor rd = new RevolvingDoor();
        GarageDoor gd = new GarageDoor();

        rd.makeSecure();
        gd.makeSecure();

        // interfaces
        System.out.println("=== interfaces ===");
        CellPhone cp = new CellPhone();
        cp.send();
        cp.receive();

        // iterators
        System.out.println("=== iterators ===");

        HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
        hm.put(1, 11);
        hm.put(2, 12);
        hm.put(3, 13);
        System.out.println("HashMap key " + 2 + " has value " + hm.get(2));
        System.out.println("one key from HashMap: " + hm.entrySet().iterator().next().getKey());
//        Iterator<Map.Entry<Integer, Integer>> hmIter = hm.entrySet().iterator();
//        for(Map.Entry<Integer, Integer> hmEntry = hmIter.hasNext() ? hmIter.next(): null; hmEntry != null; hmEntry = hmIter.hasNext() ? hmIter.next() : null){
//            System.out.println("HashMap has key " + hmEntry.getKey() + " and value " + hmEntry.getValue());
//        }
        for(Iterator<Map.Entry<Integer, Integer>> hmIter = hm.entrySet().iterator(); hmIter.hasNext();){
            Map.Entry<Integer, Integer> hmEntry = hmIter.next();
            System.out.println("HashMap has key " + hmEntry.getKey() + " and value " + hmEntry.getValue());
            //if(hmEntry.getValue() % 2 == 0){ hm.remove(hmEntry.getValue()); }
            if(hmEntry.getValue() % 2 == 0){ hmIter.remove(); }
        }
        System.out.println("HashMap size after removing pass: " + hm.size());

        TreeMap<Integer, Integer> tm = new TreeMap<Integer, Integer>();
        tm.put(1, 11);
        tm.put(2, 12);
        tm.put(3, 13);
        System.out.println("TreeMap key " + 2 + " has value " + tm.get(2));
        System.out.println("one key from TreeMap: " + tm.entrySet().iterator().next().getKey());
        System.out.println("first key from TreeMap: " + tm.firstKey());
        for(Iterator<Map.Entry<Integer, Integer>> tmIter = tm.entrySet().iterator(); tmIter.hasNext();){
            Map.Entry<Integer, Integer> tmEntry = tmIter.next();
            System.out.println("TreeMap has key " + tmEntry.getKey() + " and value " + tmEntry.getValue());
            if(tmEntry.getValue() % 2 == 0){ tmIter.remove(); }
        }
        System.out.println("TreeMap size after removing pass: " + tm.size());

        HashSet<Integer> hs = new HashSet<Integer>();
        hs.add(1);
        hs.add(2);
        hs.add(3);
        System.out.println("HashSet contains key 2: " + hs.contains(2));
        System.out.println("one key from HashSet: " + hs.iterator().next());
        for(Iterator<Integer> hsIter = hs.iterator(); hsIter.hasNext();) {
            int hsKey = hsIter.next();
            System.out.println("HashSet has key " + hsKey);
            if (hsKey % 2 == 0) { hsIter.remove(); }
        }
        System.out.println("HashSet size after removing pass: " + tm.size());


//        HashMap<Integer, Integer> hm2 = new HashMap<>();
//        hm2.put(3, 13);
//        hm2.put(5, 15);
//        hm2.put(8, 18);
//        hm2.put(9, 19);
//        Iterator<Map.Entry<Integer, Integer>> hm2Iter = hm2.entrySet().iterator();
//        while(hm2Iter.hasNext()){
//            Map.Entry<Integer, Integer> curr = hm2Iter.next();
//            System.out.println("hm2 entry has key " + curr.getKey() + " and value " + curr.getValue());
//        }

        // comparator
        System.out.println("=== comparator ===");
        TreeSet<Rectangle> rectTm = new TreeSet<Rectangle>(new RectangleComparator());
            // Why did I use comparator with a __TREE__ set?
            // Because a tree (TreeMap or TreeSet) implies ordering.
            // I must either make Rectangle implement Comparable or I must provide my own Comparator.
            // For a hash (HashMap or HashSet), I must override hashCode() and equals().
            // Comparable => compareTo(Obj b)
            // Comparator => compare(Obj a, Obj b)
        rectTm.add(new Rectangle(2,2));
        rectTm.add(new Rectangle(2,2));
        rectTm.add(new Rectangle(2, 3));
        rectTm.add(new Rectangle(3, 5));
        System.out.println(rectTm.toString());

        // generics
        System.out.println("=== generics ===");
        TrieLowerAlpha<Integer> trie = new TrieLowerAlpha<Integer>();
        trie.insert("ab", 3);
        trie.insert("abcd", 5);
        trie.insert("abxy", 6);
        System.out.println(trie.get("ab"));
        System.out.println(trie.get("abc"));
        System.out.println(trie.get("ef"));

        // parser
        System.out.println("=== parser ===");
        MathExp exp = new MathExp("1 + 2 + 10");
        System.out.println("exp:" + exp.eval());

        // equals, hashcode (and isInstanceOf?)
        System.out.println("=== equals, hashcode ===");

        // checked, unchecked exceptions
        System.out.println("=== checked, unchecked exceptions ===");
        //DoNotLikeThree.DislikeThreeChecked(3); // compile error: unhandled exception
        try{ DoNotLikeThree.DislikeThreeChecked((3)); } catch(Exception e){ e.printStackTrace(); }

        //DoNotLikeThree.DislikeThreeUnchecked(3); // compiles (but program will stop at this point)
        try{ DoNotLikeThree.DislikeThreeUnchecked((3)); } catch(RuntimeException e){ e.printStackTrace(); } // We can still catch unchecked exceptions!

        // arraylist
        System.out.println("=== arraylist ===");
        // how to add element 3 when element 1 does not exist yet?
        // how to check if element 3 exists?
        ArrayList<Integer> interestingInts = new ArrayList<>();
        interestingInts.add(3);
        //interestingInts.set(1, 4); // exception: index out of bounds
        interestingInts.ensureCapacity(10); // doesn't help
        //interestingInts.set(1, 4); // exception: index out of bounds

        interestingInts.clear();
        for(int i = 0; i < 10; i++){
            interestingInts.add(0);
        }
        interestingInts.set(1, 4);
        System.out.println(interestingInts.get(1));

        // AvlTree
        System.out.println("=== AvlTree ===");
        AvlTree<String> avl = new AvlTree<>();
        avl.insert(3, "hello");
        avl.insert(8, "goodbye");
        avl.insert(1, "a");
        avl.insert(4, "b");
        avl.insert(11, "c");
        avl.insert(31, "d");
        avl.insert(12, "e");
        avl.insert(23, "f");
        avl.insert(16, "g");
        avl.insert(84, "h");
        avl.insert(35, "i");
        avl.insert(28, "j");
        System.out.println(avl.get(3));
        System.out.println(avl.get(11));
        System.out.println(avl.get(23));
        System.out.println(avl.getKeys());
        System.out.println("avl height:" + avl.getHeight());
        System.out.println(avl.remove(4));
        System.out.println(avl.getKeys());

        // Heap
        System.out.println("=== Heap ===");
        Heap<Integer> minHeap = new Heap<>(Heap.Direction.MIN);
        minHeap.insert(12);
        minHeap.insert(8);
        minHeap.insert(3);
        minHeap.insert(21);
        minHeap.insert(36);
        System.out.println("heap min:" + minHeap.peek());
        while(minHeap.size() > 0){
            System.out.print(minHeap.remove() + " ");
        }
        System.out.println();

        // Binary Search with SGE, BLE
        System.out.println("=== Binary Search with SGE, BLE ===");
        ArrayList<Integer> arryForBinary = new ArrayList<>();
        arryForBinary.add(4);
        arryForBinary.add(6);
        arryForBinary.add(12);
        arryForBinary.add(22);
        arryForBinary.add(34);
        int tmp = 8; String tmpDir = "SGE";
        System.out.println("Searching for " + tmpDir + " " + tmp + " found " + arryForBinary.get(ArrayListUtils.binarySearch(arryForBinary, tmp, tmpDir)));



    }
}

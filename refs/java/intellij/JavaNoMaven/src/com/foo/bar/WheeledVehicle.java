package com.foo.bar;

public class WheeledVehicle {
    int numWheels;
    int numSeats;

    WheeledVehicle(int nW, int nS){
        numWheels = nW;
        numSeats = nS;
    }

    public String toString(){
        return "numWheels:" + numWheels + " numSeats:" + numSeats;
    }
}

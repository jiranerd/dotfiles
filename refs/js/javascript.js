
// run with:
// $ node javascript.js

// to start the node repl and then execute script, do:
// $ node
// > .load javascript.js

// function declaration (hoisted):
function add(a, b){
	return a + b;
}

// anonymous function expression (only var is hoisted):
var subtract = function(a, b){
	return a - b;
}

// named function expression (only var is hoisted):
var factorial = function fact(a){
	if(a == 1){
		return a;
	}
	else{
		return a * fact(a-1);
	}
}

var x = 8;
console.log('factorial of ' + x + ': ' + factorial(x));

// immediately-invoked function expression (IIFE):

(function(){
	var foo = 'foo';
	console.log(foo);
})
// foo is not visible here;
//foo; // error;

// using an IIFE to save state via closure (this code will run):
var iter = 0;
var showIter = function(){ console.log('iter is: ' + iter); };
showIter(); // 0
iter++;
showIter(); // 1

showIter = (function(lockedIter){
	return function(){ console.log('iter is: ' + lockedIter); };
})(iter);
showIter(); // 1
iter++;
showIter(); // 1

// another way to create a closure without using an IIFE:
var makeClosure = function(lockedIter){
	return function(){ console.log('iter is: ' + lockedIter); };
};
showIter = makeClosure(iter);
showIter(); // 2
iter++;
showIter(); // 2

// event emitter, listener;
var events = require('events');
var emitter = new events.EventEmitter();
emitter.on('eventC', function(){ console.log('responding to eventC'); });
emitter.emit('eventC');
// see here for how to "share" an emitter between two objects:
// https://stackoverflow.com/questions/26465358/how-to-make-an-eventemitter-listen-to-another-eventemitter-in-node-js

// using events and state-saving closures together:
var events = require('events');
var emitter = new events.EventEmitter();

var i = 0;
emitter.on('eventA', function(){ console.log('responding to eventA, i is ' + i); });
emitter.emit('eventA');
i++;
emitter.emit('eventA');

(function(lockedI){
	emitter.on('eventB', function(){ console.log('responding to eventB, i is ' + lockedI); });
})(i);
emitter.emit('eventB');
i++;
emitter.emit('eventB');
// surely there must be an easier way to do this?

// another event and state-saving example (won't run without web DOM):
// change this:
//for(var i = 0; i < elems.length; i++){
//	elems[i].addEventListener('click', function(e){
//		e.preventDefault();
//		console.log('I am link #' + i);
//	}, 'false');
//}
//// to this:
//for(var i = 0; i < elems.length; i++){
//	(function(lockedInIndex){
//		elems[i].addEventListener('click', function(e){
//			e.preventDefault();
//			console.log('I am link #' + lockedInIndex);
//		}, 'false');
//	})(i);
//}


// misc

var getThree = function(){ return 3; }
var a = getThree;
var b = getThree();
var c = (getThree);
var d = (getThree)();

console.log(a);
console.log(b);
console.log(c);
console.log(d);

var e = (function(){ return 4; }); // function name 'e' is inferred;
var f = (function(){ return 4; })();

console.log(e);
console.log(f);





























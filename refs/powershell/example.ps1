#requires -PSEdition Core
# for scripts that require regular powershell, use #requires -PSEdition Desktop
# https://stackoverflow.com/questions/57704679/use-pwsh-instead-of-ps1-extension-for-powershell-core-scripts

# all powershell scripts must have extension .ps1

# by default, powershell loads a user-specific script from C:\Users\$env:UserName\Documents\WindowsPowershell\Microsoft.PowerShell_profile.ps1

# on a fresh system, scripts will be disabled.
#PS> Set-ExecutionPolicy RemoteSigned # safer than Unrestricted
# "touch"
# echo $null >> filename

# to alias which to something useful and add it to profile:
# "`nNew-Alias which get-command" | add-content $profile

# does the same thing as adding this to profile script:
# New-Alias which get-command

# open a file in a text editor:
#PS> start myFile.txt
#PS> notepad++.exe myFile.txt

# append to path
# (current session only, can put this in profile script. useful for user-installed binaries.)
#PS> $env:path += ";C:\Users\$env:UserName\bin\npp\npp.7.7.1.bin"

echo ""
echo $env:profile
echo $PSScriptRoot
cd $PSScriptRoot # required for remainer of script samples to run

echo $env:UserName
echo $env:path


# strings with double quotes will replace dollar sign vars.
$a = 5
"a has value $a"

cat .\example.ps1 | Select-String -Pattern aeaeae
# aeaeae

cd .\text # persists after script exits
ls
cd .\..

# find file using regex (just use Everything?)

Get-Childitem -Path . -File -Recurse -ErrorAction SilentlyContinue -Include *.txt
function FindFile {$foo = $args[1]; Get-Childitem -Path $args[0] -File -Recurse -ErrorAction SilentlyContinue | Where-Object {$_.name -match "{0}" -f $foo } | select FullName }
findfile . txt # outputs multiple lines if run from script. not sure why.

# find string using regex
function FindText {Get-Childitem -Path $args[0] -File -Recurse -ErrorAction SilentlyContinue | Select-String $args[1] | select path, linenumber, line }
FindText . oo


# show function contents
echo "FindFile definition:"
(get-command findfile).definition
echo ""

# curl
Invoke-RestMethod -uri "http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22"
echo ""


# launch an editor
#PS> start myfile.txt





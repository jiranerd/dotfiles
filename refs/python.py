

# remember that python may be installed on your system as python3;


# single quotes and double quotes are interchangeable in python;
print('hello')
print("hello")

a = 'text'
print('hello ' + a)

# todo: arrays;
# todo: hashmaps;

# todo: parsing a csv;
# todo: requesting and parsing JSON;

# todo: defining a function;
# todo: defining an object;

